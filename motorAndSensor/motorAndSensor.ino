#include "motorController.h"
#include "sensorArray.h"
#include <Wire.h>
sensorArray mySensors;

//volatile boolean taskDone = false;
//int taskNumber = 0;
Motor motor1(7, 26, 22);
Motor motor2(8, 27, 50);
Motor motor3(4, 28, 24);
Motor motor4(5, 29, 25);

motorController myController(motor1, motor2, motor3, motor4);


void setup() {
  Serial.begin(9600);
   testCheck();
  mySensors.sensorSetUp();


  pinMode(43, OUTPUT);
  digitalWrite(43, LOW);
  testCheck();


}

void loop() {


  Serial.print("Back: ");
  Serial.println(mySensors.getSensor1()); //back
  Serial.print("Right: ");
  Serial.println(mySensors.getSensor2()); //right
  Serial.print("Front: ");
  Serial.println(mySensors.getSensor3()); //front
  Serial.print("Left: ");
  Serial.println(mySensors.getSensor4()); //left
  Serial.println();



  //  myController.forwards();
  delay(2000);






}

void testCheck() { // FOR TESTING NOT NEEDED
  byte count = 0;

  for (byte i = 1; i < 120; i++)
  {
    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0)
    {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      count++;
      delay (1);  // maybe unneeded?
    } // end of good response
  } // end of for loop
  Serial.println ("Done.");
  Serial.print ("Found ");
  Serial.print (count, DEC);
  Serial.println (" device(s).");

  delay(1000);
}
