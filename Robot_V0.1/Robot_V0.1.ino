#include <SoftwareSerial.h>
#include <Nextion.h>


#define RxD 5
#define TxD 6

const int LED = 13;
SoftwareSerial nextion(RxD, TxD);

Nextion myNextion(nextion, 9600);

void setup() {
  pinMode(LED,OUTPUT);
  Serial.begin(9600);
  myNextion.init();

}

void loop() {
int led_val = myNextion.getComponentValue("Magnet");
Serial.println(led_val);

if(led_val==1)
{
  digitalWrite(LED, HIGH);
  myNextion.setComponentText("mag", "ON");
}
else
{
  digitalWrite(LED, LOW);
  myNextion.setComponentText("mag", "OFF");
}
}


