#include "Arduino.h"
#include "sensorArray.h"
#include <Wire.h>


sensorArray::sensorArray() {

}

sensorArray::sensorSetUp() {
  pinMode(18, OUTPUT);
  digitalWrite(18, LOW);
  pinMode(17, OUTPUT);
  digitalWrite(17, LOW);
  pinMode(16, OUTPUT);
  digitalWrite(16, LOW);
  pinMode(14, OUTPUT);
  digitalWrite(14, LOW);
  pinMode(35, OUTPUT);
  digitalWrite(35, LOW);

  delay(500);

  Wire.begin();



//  pinMode(15, INPUT);
//  delay(150);
//  sensor2.init(true);
//  delay(100);
//  sensor2.setAddress((uint8_t)22);
//  
//  pinMode(16, INPUT);
//  delay(150);
//  sensor.init(true);
//  delay(100);
//  sensor.setAddress((uint8_t)25);
//  
//  pinMode(17, INPUT);
//  delay(150);
//  sensor3.init(true);
//  delay(100);
//  sensor3.setAddress((uint8_t)28);
//  
//  pinMode(14, INPUT);
//  delay(150);
//  sensor4.init(true);
//  delay(100);
//  sensor4.setAddress((uint8_t)31);
//  
//  pinMode(47, INPUT);
//  delay(150);
//  sensor5.init(true);
//  delay(100);
//  sensor5.setAddress((uint8_t)37);
  

  pinMode(15, INPUT);
  delay(150);
  sensor2.init(true);
  delay(100);
  sensor2.setAddress((uint8_t)22);
  
  pinMode(16, INPUT);
  delay(150);
  sensor.init(true);
  delay(100);
  sensor.setAddress((uint8_t)25);
  
  pinMode(17, INPUT);
  delay(150);
  sensor3.init(true);
  delay(100);
  sensor3.setAddress((uint8_t)28);
  
  pinMode(14, INPUT);
  delay(150);
  sensor4.init(true);
  delay(100);
  sensor4.setAddress((uint8_t)31);
  
  pinMode(35, INPUT);
  delay(150);
  sensor5.init(true);
  delay(100);
  sensor5.setAddress((uint8_t)37);


  byte count = 0;



  delay(1000);


  sensor.setTimeout(500);
  sensor2.setTimeout(500);
  sensor3.setTimeout(500);
  sensor4.setTimeout(500);
  sensor5.setTimeout(500);


  sensor.startContinuous();
  sensor2.startContinuous();
  sensor3.startContinuous();
  sensor4.startContinuous();
  sensor5.startContinuous();
}

int sensorArray::getSensor1() {
  return sensor.readRangeContinuousMillimeters() / 10;
}
int sensorArray::getSensor2() {
  return sensor2.readRangeContinuousMillimeters() / 10;
}

int sensorArray::getSensor3() {
  return sensor3.readRangeContinuousMillimeters() / 10;
}

int sensorArray::getSensor4() {
  return sensor4.readRangeContinuousMillimeters() / 10;
}
int sensorArray::getSensor1MM() {
  return sensor.readRangeContinuousMillimeters();
}
int sensorArray::getSensor2MM() {
  return sensor2.readRangeContinuousMillimeters();
}

int sensorArray::getSensor3MM() {
  return sensor3.readRangeContinuousMillimeters();
}
int sensorArray::getSensor4MM() {
  return sensor4.readRangeContinuousMillimeters();
}

int sensorArray::getSensor5MM() {
  return sensor5.readRangeContinuousMillimeters();
}

VL53L0X sensorArray::getBack() {
  return sensor;
}



