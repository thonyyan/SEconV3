#include <IRremote.h>
#include "motorController.h"
#include "sensorArray.h"
#include <Servo.h>
#include <Nextion.h>
#include <SoftwareSerial.h>

#define RLS 19
#define LLS 2
#define BOOTY 3
#define debounceTime 250
#define ROUTE_C 4
#define ROUTE_B 2
#define ROUTE_A 1
#define BNO055_SAMPLERATE_DELAY_MS (100)
Servo myservo;

//IR code
const PROGMEM uint8_t recv_pin = 30; //Pin no.
IRrecv ir_rx(recv_pin);             //Constructs IRrecv obj. to pin specified
uint8_t  routeNo = 0;                //Rest of code will reference routeNo.
decode_results results;

//Wheel Turner
const int stepPin = 9;
const int dirPin = 33;
const int enPin = 34;

sensorArray mySensors;
//---------------construction of individual motors(PWM,digital, digital)---------------

int speed1 = 176;
int speed2 = 188;
int speed3 = 120;
int speed4 = 191;


Motor motor1(7, 26, 22, speed1  / 2 ); //top port even 100
Motor motor2(8, 27, 50, speed2  / 2 ); //top starbird even 125
Motor motor3(4, 28, 24, speed3  / 2  ); //bottom port even 70
Motor motor4(5, 29, 25, speed4  / 2  ); //bottom starbird even 129

//----------------------construction of motor controllers---------------------
motorController myController(motor1, motor2, motor3, motor4);

const int RxD = 53;
const int TxD = 52;

SoftwareSerial nextion(RxD, TxD);
Nextion myNextion(nextion, 9600);

volatile int taskNum;
bool on = 1;

Adafruit_BNO055 bno = Adafruit_BNO055(55);
sensor_t sensor;

void setup() {
  Serial.begin(9600);

  myNextion.init();


  //taskOne();

  bno.getSensor(&sensor);
  bno.begin();
  mySensors.sensorSetUp();

  pinMode(RLS, INPUT_PULLUP);
  pinMode(LLS, INPUT_PULLUP);
  pinMode(BOOTY, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(LLS), changeTask, FALLING);
  attachInterrupt(digitalPinToInterrupt(RLS), changeTask, FALLING);
  attachInterrupt(digitalPinToInterrupt(BOOTY), changeTask, FALLING); // interrupters

  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(enPin, OUTPUT);
  digitalWrite(enPin, LOW); //wheel spinner

  pinMode(12, OUTPUT);

  myservo.writeMicroseconds(1500);
  myservo.attach(6);

  taskNum = 17;

}

void loop() {
      Serial.print("Back: ");
      Serial.println(mySensors.getSensor1()); //back
      Serial.print("Right: ");
      Serial.println(mySensors.getSensor2()); //right
      Serial.print("Front: ");
      Serial.println(mySensors.getSensor3()); //front
      Serial.print("Left: ");
      Serial.println(mySensors.getSensor4()); //left
      Serial.println();


    delay(1000);
  //  align();
  if (on) {
    switch (taskNum) {
      case 0:
        taskOne();
        break;
      case 1: // taskNum = 1
        testing2();
        taskNum = 100;
        break;
      case 2:// taskNum = 2
        taskThree(routeNo & ROUTE_A);
        break;
      case 3:// taskNum = 3
        myController.haults();
        delay(200);
        align();
        taskNum++;
        break;
      case 4: // taskNum = 4
        taskFour();
        break;
      case 5: // taskNum = 5
        taskFive();
        break;
      case 6: // taskNum = 6
        taskSix(routeNo & ROUTE_B);
        break;
      case 7: // taskNum = 7
        taskSeven();
        break;
      case 8: // taskNum = 8
        taskEight(routeNo & ROUTE_B);
        break;
      case 9: // taskNum = 9
        myController.haults();
        delay(200);
        align();
        myController.haults();
        wheelAlign();
        myController.haults();
        taskNum++;
        break;
      case 10: // taskNum = 10
        myController.rotateRight();
        delay(50);
        taskNine();
        break;
      case 11: // taskNum = 11
        taskTen();
        break;
      case 12: // taskNum =12
        taskEleven();
        break;
      case 13: // taskNum =13
        taskTwelve();
        taskNum = 100;
        break;
      case 14: // taskNum =14
        taskThirteen();
        break;
      case 15: // taskNum =15
        myController.haults();
        delay(200);
        align();
        break;
      case 16: // taskNum =16
        taskFourteen(routeNo & ROUTE_C);
        break;
      case 17:
        startScreen();
        taskNum = 0
        break;
      default:
        myController.haults();
        break;

    }

  }


}

void ing2(){
  moveBackward();
  delay(1000);
  moveForward();
  delay(500);
  moveLeft();
  delay(2000);
}

void testing() {
  digitalWrite(12, HIGH);
  myservo.write(140);
  delay(3000);
  myservo.write(82);
  delay(3000);

  moveForward();
}

void taskOne() {
  Serial.println(F("IR RDY"));
  uint32_t millisOnWait = millis();              // When did we start waiting?
  ir_rx.enableIRIn();                            // Start the receiver

  while(routeNo != 255){
    routeNo = getIRcmd();                        // Best-of-three error correction
    ir_rx.resume();                              // Resumes reading for IR
    myNextion.setComponentValue("va0", routeNo); // Hey, screen. This is what's up.
    
    if(routeNo = 255) millisOnWait = millis();   // Keep waiting?
    if(routeNo = 127){                           // Oh, timeout?
      if( (millis() - millisOnWait) > 10000){    // Did we wait too long?
        Serial.print(F("WARN: RECV TIME OUT")); // oshi--
        routeNo = 0;                             // Set default routeNo.
        break;                                   // Escape from while.
      }
    }
  }
  
  Serial.print(F("Recv code: "));               // Print for use when running
  Serial.println(routeNo);                      // on a tethered config.
}

void changeTask() {
  //OLDCODE -jc
  /*if (millis() - LBP > 60) { // 60
    taskNum++;
    LBP = millis();
    }
    Serial.print(F("Interrupt! taskNum:"));
    Serial.println(taskNum);*/
  switch (taskNum) { //Only inc if it's a case we want it to inc on.
    case 1:
      taskNum++;
      break;

    case 12:
      taskNum++;
      break;

    case 16:
      taskNum++;
      break;

    default:
      break;
  }
}

void taskTwo(int firstBit) {
  if (firstBit) {
    if (mySensors.getSensor1() < 15) {
      myController.forwards();
    }
    else if (mySensors.getSensor1() > 25) {
      myController.backwards();
    }
    else {
      moveRight();
    }
  }
  else {
    if (mySensors.getSensor1() < 15) {
      myController.forwards();
    }
    else if (mySensors.getSensor1() > 25) {
      myController.backwards();
    }
    else {
      moveLeft();
    }
  }
}

void taskThree(int firstBit) {
  if (firstBit) {
    while ((mySensors.getSensor2() < 48 && taskNum == 2) || (mySensors.getSensor2() > 100)) {
      if (mySensors.getSensor1() < 15 ) {
        moveForward();
      }
      else if (mySensors.getSensor1() > 27) {
        moveBackward();
      } else {
        moveLeft();
      }
    }

  }
  else {
    while ((mySensors.getSensor4() < 48 && taskNum == 2)  || (mySensors.getSensor2() > 100)) {
      if (mySensors.getSensor1() < 15 ) {
        moveForward();
      }
      else if (mySensors.getSensor1() > 27) {
        moveBackward();
      }
      else {
        moveRight();
      }
    }
  }
  taskNum++;

}

void taskFour() {
  unsigned long current = millis();
  unsigned long previous = current;
  unsigned long interval = 2500;

  //  unsigned long current = millis();
  while (taskNum == 4) {
    if (current - previous < interval) {
      moveForward();
    }
    else {
      myController.haults();
      taskNum++;
    }
    current = millis();
  }
}

void taskFive() {
  while (taskNum == 5) {
    if (mySensors.getSensor3() >= 52) {
      moveForwardAdj();
    }
    else {
      myController.haults();
      taskNum++;
    }
  }
}

void taskSix(int secondBit) {
  boolean firstTask = 1;
  while (taskNum == 6) {
    if (secondBit) {
      if (mySensors.getSensor2() >= 18 && firstTask) {
        moveRight();
      }
      else {
        firstTask = 0;
        if (mySensors.getSensor2() <= 20) {
          moveLeft();
        }
        else {
          myController.haults();
          taskNum++;
        }
      }
    }
    else {
      if (mySensors.getSensor4() >= 18 && firstTask) {
        moveLeft();
      }
      else {
        firstTask = 0;
        if (mySensors.getSensor4() <= 20) {
          moveRight();
        }
        else {
          myController.haults();
          taskNum++;
        }
      }
    }
  }
}

void taskSeven() {
  while (taskNum == 7) {
    if (mySensors.getSensor3() > 11) {
      moveForward();
    }
    else {
      taskNum++;
    }
  }
}

void taskEight(int secondBit) {
  if (secondBit) {
    while (taskNum == 8) {
      if (mySensors.getSensor2() > 47 && mySensors.getSensor2() < 50) {
        taskNum++;
      }
      else {
        moveLeft();
      }
    }
  }
  else {
    while (taskNum == 8) {
      if (mySensors.getSensor4() > 44 && mySensors.getSensor4() < 50) {
        taskNum++;
      }
      else {
        moveRight();
      }
    }
  }
}

void taskNine() {
  bool wheelTurned = 0;
  while (!wheelTurned) {

    if (mySensors.getSensor3MM() >= 30) {
      moveForward();
    }
    else {
      myController.haults();
      myController.diagonalRightUp();
      delay(400);
      digitalWrite(dirPin, HIGH); //Changes the rotations direction
      // Makes 5 full cloclwisecycle rotations as in the competition
      for (int x = 0; x < 1000; x++) {
        digitalWrite(stepPin, HIGH);
        delayMicroseconds(500);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(500);
      }
      wheelTurned = 1;
      taskNum++;
    }
  }
}

void taskTen() {
  while (taskNum == 11) {
    if (mySensors.getSensor1() > 8) { // change this parameter later
      moveBackward();
    }
    else {
      myController.haults();
      taskNum++;
    }
  }
}

void taskEleven() { // add to turn on the magnet
  digitalWrite(12, HIGH);
  myservo.write(140);
}// taskNum = 10 when front limit switch is hit



void taskTwelve() {

  myservo.write(83);
  delay(2500);

  unsigned long current = millis();
  unsigned long previous = current;
  unsigned long interval = 3500;
  unsigned long difference = current - previous;
  while (taskNum == 13) {
    if (mySensors.getSensor4() < 47) {
      moveRight();
    }
    else if (mySensors.getSensor2() < 47) {
      moveLeft();
    }
    else if (mySensors.getSensor1() < 27 && difference > interval) {
      myController.haults();
      taskNum++;
    }
    else {
      moveBackwardAdj();
    }
    current = millis();
    difference = current - previous;
  }
}

void taskThirteen() {
  unsigned long current = millis();
  unsigned long previous = current;
  unsigned long interval = 2000;

  while (taskNum == 14) {
    if (current - previous < interval) {
      myController.backwardsUp();
    }
    else if (mySensors.getSensor1() < 17 ) {
      moveForward();
    }
    else {
      taskNum++;
    }
  }
}

void taskFourteen(int thirdBit) {
  if (thirdBit) {
    while (taskNum == 16) {
      if (mySensors.getSensor1() < 17) {
        moveForward();
      }
      else if (mySensors.getSensor1() > 27) {
        moveBackward();
      } else {
        moveRight();
      }
    }
  }
  else {
    while (taskNum == 16) {
      if (mySensors.getSensor1() < 17 ) {
        moveForward();
      }
      else if (mySensors.getSensor1() > 27) {
        moveBackward();
      }
      else {
        moveLeft();
      }
    }
  }
}

void wheelAlign() {
  bool wheelAlign = 0;
  while (!wheelAlign) {
    if (mySensors.getSensor2MM() > 485 && mySensors.getSensor4MM() > 500) {
      pinMode(43, OUTPUT);
      digitalWrite(43, LOW);
      wheelAlign = 1;
    }
    else if (mySensors.getSensor2MM() < 490) {
      myController.leftAdj();
    }
    else {
      myController.rightAdj();
    }
  }
}

uint8_t readIR() {
  uint32_t startMillis = millis() ; // Time for 1s per attempt

  while (true) {                           // Busywait for a command for 1000ms
    if ((ir_rx.decode(&results))) {        // Did we decode a command?
      if (results.decode_type == IEEE) {   // No, really; did we? Rejects garbled cmds.
        return results.value;
      }
      return 128;                          // FAIL: command was garbled.
    }
    if (millis() - startMillis > 250) {    // Have we waited for too long?
      return 127;                          // FAIL: waited over 1s to decode.
    }
  }
}

uint8_t getIRcmd()
{
  uint8_t retVal = 0;                        //Will remain zero if no commands recv; failsafe route.
  uint8_t inputBuf;                          //Temporary storage indicating the path received.
  uint8_t validCmdsRecv = 0;                 //Inc'd when cmd recv'd is valid (0~7 or 255)
  uint8_t recvTimeouts = 0;                  //Inc'd when cmd recv times out
  uint8_t numCounts[10] = {0};               //Creates 9-wide array, num of received commands, idx=cmd
                                             //with TWO exceptions: 255 mapped to 8, timeout mapped to 9

  //Gets three valid signals from IR and uses them to tally "votes" for which command was received.
  //Alternatively, returns 127 early if fails to recv for ten times.
  while (validCmdsRecv < 3){
    inputBuf = readIR();                     // Gets last byte received over IR.
    if(inputBuf < 8)    validCmdsRecv++;     // Was it a valid route?
    if(inputBuf == 255) validCmdsRecv++;     // Was it a valid wait?
    if(inputBuf == 127) recvTimeouts++;      // Or did we just time out?

    if(recvTimeouts < 9){                    // Forget this, I'm going home.
      Serial.println(F("WARN: getIRcmd() timeout! Returning 127..."));
      return 127;
    }
    
    Serial.print(F("RECV "));               // Spits out command received for the sake of being
    Serial.print(i);                        // able to actually debug this godforsaken pile of
    Serial.print(F(": "));                  // electric spaghetti that nobody else bothers to
    Serial.println(inputBuf);               // debug over serial anyways. My conscience is clear.
    
    ir_rx.resume();                          // Keep listening for more IR cmds!
    if (inputBuf < 8) numCounts[inputBuf]++; // Nine states (0-7, 255); others are received in error
    if (inputBuf == 256) numCounts[8]++;     // Inc slot 8 for special case 255.
  }

  //Runs through the nine commands, updating retVal when a higher value is found.
  for (uint8_t i=1; i<9; i++) {
    if (numCounts[retVal] < numCounts[i]) retVal = i;
  }

  //Off to see the wizard...
  if(retVal==8) return 255; //Special handling for case 
  return retVal;
}

void moveRight() {
  sensors_event_t event;
  bno.getEvent(&event);
  int orientation = event.orientation.x;
  int mod = 0;

  //    Serial.println((float)event.orientation.x);

  if (orientation > 330) {
    mod = orientation - 360;
  } else {
    mod = orientation % 360;
  }

  if (mod < -4) {
    myController.rotateRight();
    Serial.println("right");
    Serial.println(mod);
  }
  else if (mod > 4) {
    myController.rotateLeft();
    Serial.println("left");
    Serial.println(mod);
  }
  else {
    myController.right();
  }

}

void moveLeft() {
  sensors_event_t event;
  bno.getEvent(&event);
  int orientation = event.orientation.x;
  int mod = 0;

  //    Serial.println((float)event.orientation.x);

  if (orientation > 330) {
    mod = orientation - 360;
  } else {
    mod = orientation % 360;
  }

  if (mod < -4) {
    myController.rotateRight();
    Serial.println("right");
    Serial.println(mod);
  }
  else if (mod > 4) {
    myController.rotateLeft();
    Serial.println("left");
    Serial.println(mod);
  }
  else {
    myController.left();
  }

}

void moveForward() {
  sensors_event_t event;
  bno.getEvent(&event);
  int orientation = event.orientation.x;
  int mod = 0;

  //    Serial.println((float)event.orientation.x);

  if (orientation > 330) {
    mod = orientation - 360;
  } else {
    mod = orientation % 360;
  }

  if (mod < -4) {
    myController.rotateRight();
    Serial.println("right");
    Serial.println(mod);
  }
  else if (mod > 4) {
    myController.rotateLeft();
    Serial.println("left");
    Serial.println(mod);
  }
  else {
    myController.forwards();
  }

}

void moveForwardAdj() {
  sensors_event_t event;
  bno.getEvent(&event);
  int orientation = event.orientation.x;
  int mod = 0;

  //    Serial.println((float)event.orientation.x);

  if (orientation > 330) {
    mod = orientation - 360;
  } else {
    mod = orientation % 360;
  }

  if (mod < -4) {
    myController.rotateRight();
    Serial.println("right");
    Serial.println(mod);
  }
  else if (mod > 4) {
    myController.rotateLeft();
    Serial.println("left");
    Serial.println(mod);
  }
  else {
    myController.forwardsAdj();
  }

}


void moveBackward() {
  sensors_event_t event;
  bno.getEvent(&event);
  int orientation = event.orientation.x;
  int mod = 0;

  //    Serial.println((float)event.orientation.x);

  if (orientation > 330) {
    mod = orientation - 360;
  } else {
    mod = orientation % 360;
  }

  if (mod < -4) {
    myController.rotateRight();
    Serial.println("right");
    Serial.println(mod);
  }
  else if (mod > 4) {
    myController.rotateLeft();
    Serial.println("left");
    Serial.println(mod);
  }
  else {
    myController.backwards();
  }

}

void moveBackwardAdj() {
  sensors_event_t event;
  bno.getEvent(&event);
  int orientation = event.orientation.x;
  int mod = 0;

  //    Serial.println((float)event.orientation.x);

  if (orientation > 330) {
    mod = orientation - 360;
  } else {
    mod = orientation % 360;
  }

  if (mod < -8) {
    myController.rotateRight();
    Serial.println("right");
    Serial.println(mod);
  }
  else if (mod > 8) {
    myController.rotateLeft();
    Serial.println("left");
    Serial.println(mod);
  }
  else {
    myController.backwardsUp();
  }

}

//void align() {
//  //          Serial.print("Back Right 1: ");
//  //        Serial.println(mySensors.getSensor1MM()); // Back left
//  //        Serial.print("Back Left 2: ");
//  //        Serial.println(mySensors.getSensor5MM()); // Back right
//  //        delay(500);
//  bool align = 0;
//  delay(100);
//  long sensor1 = mySensors.getSensor1MM();
//  long sensor5 = mySensors.getSensor5MM();
//  int tolerance = abs(sensor1 - sensor5);
//  while (!align) {
//    sensor1 = mySensors.getSensor1MM();
//    sensor5 = mySensors.getSensor5MM();
//    tolerance = abs(sensor1 - sensor5);
//
//    Serial.println(sensor1);
//    Serial.println(sensor5);
//    Serial.println(tolerance);
//    if (tolerance < 10) {
//      taskNum++;
//      align = 1;
//    }
//    else if (sensor1 > sensor5) {
//      myController.rotateRight();
//
//    }
//    else {
//      myController.rotateLeft();
//    }
//  }
//}

void align() {
  sensors_event_t event;
  bno.getEvent(&event);
  int orientation = event.orientation.x;
  int mod = 0;
  bool align = 0;

  //    Serial.println((float)event.orientation.x);

  if (orientation > 330) {
    mod = orientation - 360;
  } else {
    mod = orientation % 360;
  }

  while (!align) {
    sensors_event_t event;
    bno.getEvent(&event);
    orientation = event.orientation.x;


    if (orientation > 330) {
      mod = orientation - 360;
    } else {
      mod = orientation % 360;
    }

    if (mod > -4 && mod < 4) {
      align = 1;
    }
    else if (mod < -4) {
      myController.rotateRight();
      Serial.println("right");
      Serial.println(mod);
    }
    else {
      myController.rotateLeft();
      Serial.println("left");
      Serial.println(mod);
    }

  }
}

void spin180() {
  sensors_event_t event;
  bno.getEvent(&event);
  int orientation = event.orientation.x;
  bool turned = 0;

  while (!turned) {
    sensors_event_t event;
    bno.getEvent(&event);
    orientation = event.orientation.x;
    myController.rotateRight();
    if (event.orientation.x >= 175 && event.orientation.x <= 185) {
      turned = 1;
    }
  }

  myController.haults();
}






