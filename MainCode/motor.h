#ifndef motor_h
#define blinker_h

#include "Arduino.h"

class Motor
{
  public:
    Motor();
    Motor(int Speed, int forward, int backward, int PWM);
    void forward();
    void backward();
    void hault ();
    void adjustableSpeedBack(int newSpeed);
    void adjustableSpeedFord(int adjustedSpeed);

  private:
    int _Speed;
    int _forward;
    int _backward;
    int _PWM;

};
#endif

